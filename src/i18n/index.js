import english from './en';
import deutch from './de';
import french from './fr';
import italian from './it';
import spanish from './es';

export default
{
  de: deutch,
  en: english,
  es: spanish,
  fr: french,
  it: italian
};
